USE [EmployeeTimeManagementSystem]
GO
/****** Object:  Table [dbo].[BreakTypes]    Script Date: 12-06-2018 14:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BreakTypes](
	[ID] [int] NOT NULL,
	[BreakTypes] [varchar](10) NULL,
 CONSTRAINT [PK_BreakTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DateTimeLogging]    Script Date: 12-06-2018 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DateTimeLogging](
	[EmpId] [varchar](10) NULL,
	[BreakType] [int] NOT NULL,
	[StartBreakTime] [varchar](20) NULL,
	[EndBreakTime] [varchar](20) NULL,
	[Duration] [varchar](5) NULL,
 CONSTRAINT [PK_DateTimeLogging] PRIMARY KEY CLUSTERED 
(
	[BreakType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DayDateTimeLogging]    Script Date: 12-06-2018 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DayDateTimeLogging](
	[EmpId] [varchar](10) NULL,
	[StartTime] [varchar](15) NULL,
	[EndTime] [varchar](15) NULL,
	[Duration] [varchar](5) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmpInfo]    Script Date: 12-06-2018 14:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpInfo](
	[EmpId] [varchar](10) NOT NULL,
	[Fname] [varchar](10) NULL,
	[Lname] [varchar](10) NULL,
	[EmailId] [varchar](20) NOT NULL,
	[DOB] [date] NULL,
 CONSTRAINT [PK_EmpInfo] PRIMARY KEY CLUSTERED 
(
	[EmpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
